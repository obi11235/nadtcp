from setuptools import setup

setup(name='nadtcp',
      version='0.1.1',
      description='Library for TCP/IP enabled NAD amplifiers. Currently only the C338 is supported.',
      url='https://gitlab.com/mindig.marton/nadtcp',
      download_url='https://gitlab.com/mindig.marton/nadtcp/repository/archive.zip?ref=0.1.1',
      author='mindigmarton',
      license='MIT',
      packages=['nadtcp'],
      install_requires=[],
      zip_safe=True) 
